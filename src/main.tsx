import React from 'react'
import ReactDOM from 'react-dom/client'

const HELLO_BUG = import.meta.env.VITE_ENV_VAR

console.log(HELLO_BUG)

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <div>hello react</div>
  </React.StrictMode>,
)
