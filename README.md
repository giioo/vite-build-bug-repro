# Build bug repro

To reproduce, follow these instructions:

- using either `bun` or `ts-node`, run the `build.ts` file
   - e.g. `bun run ./build.ts`
- Notice that the built JS file inside of `dist/assets/` does not reference the env from `.env.production`, but rather from `.env.local` even though the `mode` has been set to `production` for the `build` function inside of `build.ts`