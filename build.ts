import { build, defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

/** Leaving the original code here that was working.

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
})

*/

const runBuild = async () => {
  await build({
    // even with mode set to `production`, the `.env.production` does not
    // get picked up
    mode: 'production',
    plugins: [react()],
  })
}

runBuild()